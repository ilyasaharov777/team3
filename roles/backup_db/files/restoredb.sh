#!/bin/bash
echo "Starting restore from" $1
aws s3 cp s3://team3db/$1 . --endpoint-url https://hb.bizmrg.com
mysql wpdb < $1
rm $1
echo "DB restore finished"
