#!/bin/bash
echo "Starting restore from" $1
aws s3 cp s3://team3mona/$1 . --endpoint-url https://hb.bizmrg.com
unzip -o -l $1
rm $1
echo "Monitoring restore finished"
