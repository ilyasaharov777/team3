#!/bin/bash
mysqldump wpdb > $(date +'%Y%m%d')"wpdb.sql"
aws s3 cp  $(date +'%Y%m%d')"wpdb.sql" s3://team3db/$(date +'%Y%m%d')"wpdb.sql" --endpoint-url https://hb.bizmrg.com
aws s3 rm s3://team3db/$(date +'%Y%m%d' -d "3 day ago")"*" --recursive --endpoint-url https://hb.bizmrg.com
rm -f $(date +'%Y%m%d')"wpdb.sql"
sudo /usr/local/bin/write-node-exporter-metric -c 'backup' -v $?
#telegram -M "*BACKUP ALERT*"$'\n'$"Backup of db server completed successfully"
