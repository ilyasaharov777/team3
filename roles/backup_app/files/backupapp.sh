#!/bin/bash
zip -9 -r $(date +'%Y%m%d')"html.zip" /var/www/html/
aws s3 cp $(date +'%Y%m%d')"html.zip" s3://team3app/$(date +'%Y%m%d')"html.zip" --endpoint-url https://hb.bizmrg.com
aws s3 rm s3://team3app/$(date +'%Y%m%d' -d "3 day ago")"*" --recursive --endpoint-url https://hb.bizmrg.com
rm -f $(date +'%Y%m%d')"html.zip"
#sudo /usr/local/bin/write-node-exporter-metric -c 'backup' -v $?
#telegram -M "*BACKUP ALERT*"$'\n'$"Backup of app server completed successfully"
