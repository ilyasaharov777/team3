#!/bin/bash
sudo zip -9 -r $(date +'%Y%m%d')"grafana.zip" /var/lib/grafana/grafana.db
sudo aws s3 cp $(date +'%Y%m%d')"grafana.zip" s3://team3mona/$(date +'%Y%m%d')"grafana.zip" --endpoint-url https://hb.bizmrg.com
sudo aws s3 rm s3://team3mona/$(date +'%Y%m%d' -d "3 day ago")"*" --recursive --endpoint-url https://hb.bizmrg.com
sudo rm -f $(date +'%Y%m%d')"grafana.zip"
sudo /usr/local/bin/write-node-exporter-metric -c 'backup' -v $?
#telegram -M "*BACKUP ALERT*"$'\n'$"Backup of mona server completed successfully"